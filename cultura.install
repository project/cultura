<?php
/**
 * @file
 * Install, update and uninstall functions for the Cultura installation profile.
 */

/**
 * Implements hook_install().
 *
 * Perform actions to set up the site for this profile.
 *
 * @see system_install()
 */
function cultura_install() {
  // Set a private file path; required by WordPress Migration.
  variable_set('file_private_path', 'sites/default/files/private-files');
  // Set public file path or it will not be in sites/default in MIT's setup.
  variable_set('file_public_path', 'sites/default/files');

  cultura_configure_pathauto();

  // Add text formats.
  require drupal_get_path('profile', 'cultura') . '/includes/formats.inc';
  $filtered_html_format = cultura_create_filtered_html_format();
  $full_html_format = cultura_create_full_html_format();

  require drupal_get_path('profile', 'cultura') . '/includes/user.inc';
  cultura_configure_accounts();
  cultura_configure_user_profile();

  require drupal_get_path('profile', 'cultura') . '/includes/content_types.inc';
  cultura_create_page_content_type();

  // Configure theme and blocks.
  require drupal_get_path('profile', 'cultura') . '/includes/theme.inc';
  cultura_configure_themes();

  require drupal_get_path('profile', 'cultura') . '/includes/roles_perms.inc';
  cultura_set_permissions_anonymous_authenticated($filtered_html_format);
  cultura_create_student_role(array($filtered_html_format));
  cultura_create_observer_role(array($filtered_html_format));
  cultura_create_guest_instructor_role(array($filtered_html_format));
  $host_role = cultura_create_host_instructor_role(array($filtered_html_format));
  cultura_create_developer_role();

  $administrator = cultura_create_administrator_user($host_role);

  // Set front page to the Discussion listing.
  variable_set('site_frontpage', 'discussions');
  // Delete the now-extraneous Discussions menu link.
  menu_link_delete(NULL, 'discussions');

  // Create a Home link in the main menu.
  $item = array(
    'link_title' => st('Home'),
    'link_path' => '<front>',
    'menu_name' => 'main-menu',
    'weight' => 0,
  );
  menu_link_save($item);

  // Update the menu router information.
  menu_rebuild();

  require drupal_get_path('profile', 'cultura') . '/includes/content.inc';
  cultura_create_about_page($administrator);

  cultura_configure_date_format();

  cultura_configure_customerror();

  cultura_configure_registration_role();

  cultura_configure_roleassign();

  cultura_configure_search404();

  cultura_configure_statistics();

  cultura_configure_jquery_update();
}

/**
 * Configures pathauto.
 *
 * Most important is the default content-type/title path.
 *
 * We also ignore far fewer words than is default, and replace underscores with
 * hyphens.  (Hyphens are also replaced with hyphens...)
 */
function cultura_configure_pathauto() {
  variable_set('pathauto_ignore_words', 'a, an, of, the, this, that, to');
  variable_set('pathauto_node_pattern', '[node:content-type]/[node:title]');
  variable_set('pathauto_node_page_pattern', '[node:title]');
  variable_set('pathauto_punctuation_hyphen', 1);
  variable_set('pathauto_punctuation_underscore', 1);
}

/**
 * Add an unambiguous, date-only format.
 *
 * Note: Alternatively, we could implement hook_date_formats().
 */
function cultura_configure_date_format() {
  $date_format = array(
    'format' => 'Y F jS, l',
    'type' => 'custom',
    'locked' => 0,
    // 'is_new' is needed because Drupal has evident brain damage.
    'is_new' => TRUE,
  );
  system_date_format_save($date_format);
}

/**
 * Configure the custom error module.
 */
function cultura_configure_customerror() {
  variable_set('customerror_403', 'If you are a student participating in this Cultura Exchange, please log  in.  If you have not yet registered, please contact your instructor for a registration link.');
  variable_set('customerror_403_title', 'Please log in to access Cultura Exchange');
  variable_set('customerror_404', 'Requested page not found.  Please <a href="/">return to the Cultura Exchange home page</a> or try the search.');
  variable_set('customerror_404_title', 'Page not found');
  variable_set('site_403', 'customerror/403');
  variable_set('site_404', 'customerror/404');

  // Configure custom error alternate for authenticated user.
  variable_set('customerror_403_authenticated', 'You do not have permission to view this page.');
  variable_set('customerror_403_authenticated_title', 'Access denied');
}

/**
 * Configure search404.
 */
function cultura_configure_search404() {
  // Go directly to resulting page if only one.
  variable_set('search404_jump', TRUE);
}

/**
 * Configure Registration Role module.
 */
function cultura_configure_registration_role() {
  $role = user_role_load_by_name('student');
  variable_set('registration_role_roles', $role->rid);
}

/**
 * Configure RoleAssign module.
 *
 * Permissions are granted in roles_perms.inc for the host instructor to
 * grant these roles (Administer users and Assign roles permissions).
 */
function cultura_configure_roleassign() {
  $assignable_roles = array(
    4 => 4,
    3 => 3,
    5 => 5,
    6 => 0,
    7 => 0,
  );
  variable_set('roleassign_roles', $assignable_roles);
}

/**
 * Configure Statistics module.
 */
function cultura_configure_statistics() {
  variable_set('statistics_count_content_views', 1);
  variable_set('statistics_count_content_views_ajax', 1);
  variable_set('statistics_day_timestamp', 1410280468);
  variable_set('statistics_enable_access_log', 1);
  variable_set('statistics_flush_accesslog_timer', '4838400');
}

/**
 * Configure JQuery update to work for Bootstrap but not break the teaser split.
 */
function cultura_configure_jquery_update() {
  variable_set('jquery_update_compression_type', 'min');
  variable_set('jquery_update_jquery_admin_version', '');
  variable_set('jquery_update_jquery_cdn', 'none');
  variable_set('jquery_update_jquery_version', '1.8');
}