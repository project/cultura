<?php
/**
 * @file
 * Enables modules and site configuration for a standard site installation.
 */

/**
 * Implements hook_form_FORM_ID_alter() for install_configure_form().
 *
 * Allows the profile to alter the site configuration form.
 */
function cultura_form_install_configure_form_alter(&$form, $form_state) {
  // Pre-populate the site name with the current year and semester.
  $form['site_information']['site_name']['#value'] = cultura_site_name();
}

/**
 * Returns current year and semester and words "Cultura Exchange".
 *
 * Also saves
 */
function cultura_site_name() {
  $semester = (date('z') < 183) ? 'Spring' : 'Fall';
  variable_set('cultura_semester', $semester);
  variable_set('cultura_year', date('Y'));
  return date('Y') . ' ' . $semester . ' ' . st('Cultura Exchange');
}

/**
 * Utility function to save a node given a node_export-style array.
 *
 * This array should be modified from node_export (nid and many other values
 * removed) if taken from node_export but this wrapper accepts this simpler
 * array and works around a node_save() quirk.
 */
function cultura_node_save($node, $author_uid) {
  $node = (object) $node;
  $node->language = LANGUAGE_NONE;
  node_object_prepare($node);
  $node->log = 'Starter content for Cultura.';
  // Set the uid that node_object_prepare() loses anyway.
  $node->uid = $author_uid;
  // Pretty sure nothing helps the revision uid, unfortunately...
  $node->revision_uid = $author_uid;
  node_save($node);
  return $node;
}

/**
 * Implements hook_form_FORM_ID_alter() for user_register_form().
 *
 * Indicate to users they should provide a pseudonym for a username.
 */
function cultura_form_user_register_form_alter(&$form, &$form_state) {
  $form['account']['name']['#description'] = t('Please provide a username by which you will be identified in discussions. (Your answers to questionnaires are always anonymous.)');
}