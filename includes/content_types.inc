<?php

/**
 * @file
 * Insert default pre-defined node types into the database.
 *
 * For a complete list of available node type attributes, refer to the node type
 * API documentation at: http://api.drupal.org/api/HEAD/function/hook_node_info.
 */

/**
 * Set options and fields for Page content type.
 */
function cultura_create_page_content_type() {
  $type = array(
    'type' => 'page',
    'name' => st('Page'),
    'base' => 'node_content',
    'description' => st("Use <em>pages</em> for your static content, such as an 'About us' page."),
    'custom' => 1,
    'modified' => 1,
    'locked' => 0,
  );
  $type = node_type_set_defaults($type);
  node_type_save($type);
  node_add_body_field($type);

  $rdf_mapping = array(
    'type' => 'node',
    'bundle' => 'page',
    'mapping' => array(
      'rdftype' => array('foaf:Document'),
    )
  );
  rdf_mapping_save($rdf_mapping);

  // Tags field already created in Cultura Discussion module.
  $help = st('List words or phrases you associate with this content, separated by commas.');
  $instance = array(
    'field_name' => CULTURA_TAGS,
    'entity_type' => 'node',
    'bundle' => 'page',
    'label' => 'Tags',
    'description' => $help,
    'widget' => array(
      'weight' => '2',
      'type' => 'taxonomy_autocomplete',
      'module' => 'taxonomy',
      'active' => 0,
      'settings' => array(
        'size' => 60,
        'autocomplete_path' => 'taxonomy/autocomplete',
      ),
    ),
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'weight' => '10',
      ),
      'teaser' => array(
        'label' => 'inline',
        'type' => 'hidden',
        'weight' => '1',
        'settings' => array(),
      ),
    ),
    'default_value' => NULL,
  );
  field_create_instance($instance);

  // Default "Page" to not be promoted and have comments disabled.
  variable_set('node_options_page', array('status'));
  variable_set('comment_page', COMMENT_NODE_HIDDEN);

  // Don't display date and author information for "Page" nodes by default.
  variable_set('node_submitted_page', FALSE);
}