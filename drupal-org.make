; Cultura Drupal Distribution
core = 7.x

; Drush Make API version
api = 2

; Uncomment to run drush make-update on this file
; projects[drupal][type] = core
; projects[drupal][version] = 7.x

defaults[projects][subdir] = contrib

; Modules
projects[auto_nodetitle][version] = "1.0"
projects[better_formats][version] = "1.0-beta2"
projects[ctools][version] = "1.15"
projects[node_clone][version] = "1.0"
projects[customerror][version] = "1.4"
projects[double_field][version] = "2.4"
projects[jquery_update][version] = "2.7"
projects[node_export][version] = "3.1"
projects[references][version] = "2.2"
projects[pathauto][version] = "1.3"
projects[publish_button][version] = "1.1"
projects[roleassign][version] = "1.2"
projects[search404][version] = "1.6"
projects[token][version] = "1.7"
projects[typogrify][version] = "1.0-rc10"
projects[uuid][version] = "1.2"
projects[views][version] = "3.20"
projects[views_data_export][version] = "3.2"
projects[webform][version] = "4.19"

projects[config_perms][version] = "2.2"
projects[fieldhelp][version] = "2.0-alpha2"
projects[jeap][version] = "1.0-alpha1"
projects[registration_role][version] = "1.2"

; Cultura modules
projects[cultura_answer][version] = "1.1"
projects[cultura_discussion][version] = "1.0"
projects[cultura_export][version] = "1.2"
projects[cultura_questionnaire][version] = "1.0"
projects[cultura_registration][version] = "1.0"

; Themes
projects[bootstrap][version] = "3.23"
projects[bootstrap][type] = "theme"

; Themes for Cultura
projects[cultura_bootstrap][version] = "1.0-beta2"
projects[cultura_bootstrap][type] = "theme"

